package com.example.mathgame

import kotlinx.android.synthetic.main.fragment_main_game.*
import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [main_game.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [main_game.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class main_game : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    internal lateinit var addButton: Button
    internal lateinit var restaButton: Button
    internal  lateinit var multiplicacionButton: Button
    internal lateinit var divisionButton: Button


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        addButton = view.findViewById(R.id.addButon)
        addButton.setOnClickListener{ goToADD()}
        restaButton= view.findViewById(R.id.restabutton)
        restaButton.setOnClickListener{ goToResta()}
        multiplicacionButton= view.findViewById(R.id.multiplicationButton)
        multiplicacionButton.setOnClickListener{ goToMultiplicacion()}
        divisionButton = view.findViewById(R.id.DivisionButton)
        divisionButton.setOnClickListener { goToDivision()}
    }

    fun goToADD(){
        val action = main_gameDirections.actionMainGameToSuma()
        view?.findNavController()?.navigate(action)
    }

    fun goToResta(){
        val action = main_gameDirections.actionMainGameToResta()
        view?.findNavController()?.navigate(action)
    }

    fun goToMultiplicacion(){
        val action = main_gameDirections.actionMainGameToMultiplicacion()
        view?.findNavController()?.navigate(action)
    }

    fun goToDivision(){
        val action = main_gameDirections.actionMainGameToDivision()
        view?.findNavController()?.navigate(action)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_game, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment main_game.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            main_game().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
